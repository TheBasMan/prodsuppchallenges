create procedure myProcedure
	@userDate varchar(20)

as

Declare @dateString varchar(8)
set @dateString = (SELECT (LEFT(@userDate,8)));
Declare @timeString varchar(4)
set @timeString = (SELECT(RIGHT(@userDate,4)));
Declare @hourString varchar(2)
set @hourString = (SELECT(LEFT(@timeString,2)));
Declare @minString varchar(2)
set @minString = (SELECT(RIGHT(@timeString,2)));
Declare @endString varchar(10)
Declare @newHourString varchar(10)
set @newHourString = @hourString+5;

if @hourString + 5 > 16
	set @endString = stuff(substring((select max([<date>]) from testTable where [<date>] like @dateString + '%'),9,12),3,0,':')
	else set @endString = @newHourString+ ':' + @minString

select sum([<vol>]*[<close>])/sum([<vol>]) as Volume_Weighted_Price
,Convert(varchar,convert(datetime, substring(@userDate,1,8)),103) as [Date]
,'Start ('+ stuff(substring(@userDate,9,4),3,0, ':') + ') - End ('+@endString + ')' as Interval
from testTable where [<date>] between @userDate and (@dateString+@newHourString+@minString)

