#README.md

Welcome to the README file for SQL challenge 1.

#Sql challenge 1

*This dataset contains pricing information for Apple Computer stock across one month in 2010. Create a SQL script to determine the Volume Weighted 
Average Price considering the ticks in a specified 5 hour interval on any specific day.*

##Table creation

###You'll find the table:
###What the table contains:

The table contains 7 nullable columns with the following headings and types:

* [<ticker>] *varchar* - stores the ticker value of the company
* [<date>] *varchar* - stores the date in string format 
* [<open>] *numeric* - stores the price at which a stock opened on a given date 
* [<high>] *numeric* - stores the maximum price the stock reached on a given date 
* [<low>] *numeric* - stores the minimum price the stock reached on a given date
* [<close>] *numeric* - stores the price at which the stock closed on a given date 
* [<vol>] *int* - stores the amount it stock that was bought or sold on a given date between the open and close times. 

##The sql code
###You'll find the code here:
[Challenge 1](https://bitbucket.org/TheBasMan/prodsuppchallenges/src/master/myProcedure.sql)

###What the code does:

Waits for the user to input a date and time in string format 'yyyymmddhhmm'

Compares the user entered date to the date in the 
file then returns the stock prices from that time plus five hours.

If the end of day is within the 5 hours then it shows prices only till the end of day

###The command to run the procedure

    exec myProcedure 'yyyymmddhhmm'

###Assumptions

* The user will input the date and time concatenated  in string format
* The close price was assumed to be the sell price as no specific sell price was specified
* If asked for 5 hours worth of data less than 5 hours before end of the day, the procedure only puts out data till the end of the day.
