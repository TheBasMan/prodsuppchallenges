USE [testDB]
GO

/****** Object:  Table [dbo].[testTable2]    Script Date: 9/18/2018 4:14:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[testTable2](
	[Date] [varchar](50) NULL,
	[Time] [varchar](50) NULL,
	[Open] [numeric](38, 10) NULL,
	[High] [numeric](38, 10) NULL,
	[Low] [numeric](38, 10) NULL,
	[Close] [numeric](38, 10) NULL,
	[Volume] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

