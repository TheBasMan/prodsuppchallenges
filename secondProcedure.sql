create procedure secondProcedure

as

select top 3 [Date],max(abs([Open]-[Close])) as theRange into #mytemp
from testTable2
group by [Date]
order by theRange desc

--select theRange as theRange, [Date] from #mytemp order by theRange desc;

--select [Date], [Time] into #newtemp
--from testTable2

select [Date], max(abs([high])) as max_high into #newtemp from testTable2 where [Date] in (select [Date] from #mytemp) 
group by [Date]
--select * from #newtemp order by max_high desc;

select High, testTable2.[Date], [Time] into #finaltemp from testTable2
join #newtemp ON testTable2.[Date] = #newtemp.[Date] and #newtemp.[max_high] = testTable2.[High] 

select #finaltemp.[Date],#mytemp.[theRange], #finaltemp.[Time] from #finaltemp
join #mytemp ON #finaltemp.[Date] = #mytemp.[Date];

drop table #mytemp;
drop table #newtemp;
drop table #finaltemp;