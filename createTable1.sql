USE [testDB]
GO

/****** Object:  Table [dbo].[testTable]    Script Date: 9/18/2018 11:14:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[testTable](
	[<ticker>] [varchar](50) NULL,
	[<date>] [varchar](50) NULL,
	[<open>] [numeric](38, 10) NULL,
	[<high>] [numeric](38, 10) NULL,
	[<low>] [numeric](38, 10) NULL,
	[<close>] [numeric](38, 10) NULL,
	[<vol>] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


