#README2.md

Welcome to the README2 file for SQL challenge 2.

#Sql challenge 2

*Create a SQL script to determine the 3 days on which there was the biggest range between opening and closing prices.*

##Table creation

###You'll find the table:

###What the table contains:
* [Date] *varchar* - stores the date in string format
* [Time ] *varchar* - stores the time in string format 
* [Open] *numeric* - stores the price at which a stock opened on a given date 
* [High] *numeric* - stores the maximum price the stock reached on a given date 
* [Low] *numeric* - stores the minimum price the stock reached on a given date
* [Close] *numeric* - stores the price at which the stock closed on a given date 
* [Volume] *int* - stores the amount it stock that was bought or sold on a given date between the open and close times. 

##The sql code

###You'll find the code here:
[Challenge 2](https://bitbucket.org/TheBasMan/prodsuppchallenges/src/master/secondProcedure.sql)

###What the code does:

Selects the 3 dates based on top 3 ranges, into a new temporary table called **#mytemp**

Selects the dates where the top 3 ranges like and find the the highs on those days, put them into a new table called **#newtemp**

Join the original table and second temporary table where both dates and max highs match, creating another temporary table called **#finaltemp**

Join **#mytemp** and **#finaltemp** where the dates match to get the desired results.

Drop all temporary tables at the end. 

###The command to run the procedure
    exec secondProcedure

###Assumptions

* The range was found by considering the difference between open and close prics.
